# LAB 2 - Monolith web application with mongo + env management

Small lab project for students to test how to configure and use a monolith web application. this project will help them understand 
the lifecyle of an environment, how to handle environment variable, database connection and even how to code JS and CSS

As always, good luck!

## Pre-Tasks
You will need a **Gitlab account** and **Heroku** with an **API_KEY**, if you dont have this, ask one of your teammates to help you ( lab1 ).

 ## Tasks
 
 ### Let's run the app ( 30% )
 As we did in lab1 , we will update the `.gitlab-ci.yml` and update the variables needed, if you dont know how, ask a teammate ( lab 1 )..
 
 If everything was done correctly, you should have a `OK` on your app with 2 codes, send them to me. 
 
 ### Let's get some data! ( 50% )
 
 Now this will be trickier, we will add data to our application, but how and where?
 
 - Find the format of the data, hidden in the sources of  the project ( first one gets `extra 5%`)
 - Once you have them generate 5 different `documents` (a registry on SQL)
 - Go to `Heroku/yourapp/resources/mlab`, create a new collection `lab2` and add your json
 - If everything is ok you should now see something different in your webapp, send me a screenshot to `luisdaniel.rubieraguzman@epfedu.fr`
 
 ### Lets beautify it :) ( 20% )
 
As now you're an expert in database, lets update that ugly table in front of your face!

- Generate a color from your last name (you can use any text to hexadecimal you want)
- Update the CSS of the project and add your code
- Send me a screenshot of your code + your style on the chrome devtools with the element changed(`extra 5%`) to `luisdaniel.rubieraguzman@epfedu.fr`
- also send me the hexadecimal result + the site you used to generate the hexadecimal
